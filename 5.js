/*Экранирование - способ размещения внутри литерала символов окончания литерала, а также непечатаемых и специальных символов или команд.
Другими словами, если в тексте нужно вставить спец-символ в специальной кодировке
или перенести строку/включить табуляцию или тп, есть возможность вставить специальный 
символ экранирования "\" и добавить буквенный/числовой символ
или группу символов. Интерпритатор восприимет такой набор символов как команду.*/


let newUser = createNewUser();
console.log(newUser);
console.log(`The LOGIN for New User is: ${newUser.getLogin()}`);
console.log(`The AGE of New User is: ${newUser.getAge()}`);
console.log(`The PASSWORD for New User is: ${newUser.getPassword()}`);

function createNewUser() { 
    const birthD = prompt("Enter your birthday as dd.mm.yyyy");
    let newUser = {
        _firstName: this.firstName = prompt("Enter your first name"),
        _lastName: this.lastName = prompt("Enter your last name"),
        _birthday: this.birthday = Date.parse(`${birthD.slice(6, 10)}-${birthD.slice(3, 5)}-${birthD.slice(0, 2)}T00:00:00.000Z`),

        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge: function () {
            return Math.trunc((Date.parse(new Date()) - this.birthday) / (1000 * 60 * 60 * 24 * 365.25));
        },
        getPassword: function () {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + (new Date(this.birthday)).getFullYear();
        },
        set firstName(value) {
            this._firstName = value;
        },
        set lastName(value) {
            this._lastName = value;
        },
        /*set birthday(value) {
            this._birthday = value;
        },*/
        get firstName() {
            return this._firstName;
        },
        get lastName() {
            return this._lastName;
        },
        get birthday() {
            return this._birthday;
        },
    };
    return newUser;   
}
